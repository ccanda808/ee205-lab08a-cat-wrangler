///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 2.0
///
/// List class
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "node.hpp"
#pragma once

using namespace std;

class DoubleLinkedList{
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;

   public:
      const bool empty() const;

      void push_front( Node* newNode );
      void push_back( Node* newNode );

      Node* pop_front();
      Node* pop_back();

      Node* get_first() const;
      Node* get_last() const;

      Node* get_next( const Node* currentNode ) const;
      Node* get_prev( const Node* currentNode ) const;

      inline unsigned int size() const{
         unsigned int n = 0;
         Node* currentNode = head;
         while(currentNode != nullptr){
            currentNode = currentNode -> next;
            n++;
         }
         return n;
      }
   
   void swap(Node* node1, Node* node2);

   const bool isSorted() const;
   void insertionSort();
};
