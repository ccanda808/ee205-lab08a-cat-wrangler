///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 2.0
///
/// Exports data about list
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "list.hpp"
#include "node.hpp"

using namespace std;

// return true if the list is empty
// false if there's anything in the list
const bool DoubleLinkedList::empty() const{
   if(head == nullptr) return true;
   else return false;
}

// Add newNode to the front of the list
void DoubleLinkedList::push_front( Node* newNode ){
   newNode -> prev = nullptr;
   if(DoubleLinkedList::empty()){
      head = newNode; tail = newNode;
   } else {
      newNode -> next = head;
      head -> prev = newNode;
      head = newNode;
   }
}

// Add newNode to the back of the list
void DoubleLinkedList::push_back( Node* newNode ){
   newNode -> next = nullptr;
   if(DoubleLinkedList::empty()){
      head = newNode; tail = newNode;
   } else {
      newNode -> prev = tail;
      tail -> next = newNode;
      tail = newNode;
   }
}

// Remove a node from the front of the list
// Return nullptr if list is empty
Node* DoubleLinkedList::pop_front(){
   if(head == tail) return nullptr;
   if(head == nullptr) return nullptr;
   else{
      Node* temp = head;
      head = head -> next;
      temp -> prev = nullptr;
      return temp;
   }
}

// Remove a node from the back of the list
// If the list is already empty return nullptr
Node* DoubleLinkedList::pop_back(){
   if(head == tail) return nullptr;
   if(tail == nullptr) return nullptr;
   else{
      Node* temp = tail;
      tail = tail -> prev;
      temp -> next = nullptr;
      return temp;
   }
}

// Return first node from list w/out making changes
Node* DoubleLinkedList::get_first() const{
   if(head == nullptr) return nullptr;
   else return head;
}

// Return last node from list w/out making changes
Node* DoubleLinkedList::get_last() const{
   if(head == nullptr) return nullptr;
   else return tail;
}

// Return the node immediately following currentNode
Node* DoubleLinkedList::get_next( const Node* currentNode ) const{
   if(currentNode -> next == nullptr) return nullptr;
   else return currentNode -> next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{
   if(currentNode -> prev == nullptr) return nullptr;
   else return currentNode -> prev;
}

void DoubleLinkedList::swap(Node* node1, Node* node2){

   if(node1 == node2) return;
   if(node1 == nullptr || node2 == nullptr) return;

   Node* node1_prev = node1 -> prev;
   Node* node2_prev = node2 -> prev;
   Node* temp;

   if(node1_prev != NULL)
      node1_prev -> next = node2;

   if(node2_prev != NULL)
      node2_prev -> next = node1;

   temp = node1 -> next;
   node1 -> next = node2 -> next;
   node2 -> next = temp;

   if(node1_prev == NULL)
      head = node2;
   else if(node2_prev == NULL)
      head = node1;

}

const bool DoubleLinkedList::isSorted() const{
   
   for(Node* i = head; i -> next != nullptr; i = i -> next){
      if(*i > *i -> next)
         return false;
   }
   return true;

}

void DoubleLinkedList::insertionSort(){

   for(Node* i = head; i -> next != nullptr; i = i -> next){
      Node* min = i;

      for(Node* j = i -> next; j != nullptr; j = j -> next){
         if(*min > *j)
            min = j;
      }

      DoubleLinkedList::swap(i, min);
      i = min;

   }

}

